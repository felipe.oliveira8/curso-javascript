console.log(`Trabalhando com listas`);

// const salvador = `Salvador`;
// const saoPaulo = `São Paulo`;
// const rioDeJaneiro = `Rio de Janeiro`;

const listaDeDestinos = new Array(
    `Salvador`,
    `São Paulo`,
    `Rio de Janeiro`

); //criando listas com array
listaDeDestinos.push(`Curitiba`); //Adicionado um elemento na lista
console.log("Destinos possveis:");
// console.log(salvador, saoPaulo, rioDeJaneiro);
console.log(listaDeDestinos);
console.log(listaDeDestinos.length);


listaDeDestinos.splice(1 , 1); //Removendo elemento 1 do array
console.log(listaDeDestinos);

console.log(listaDeDestinos[1]) //Exibindo elemento na posição 1

console.log(listaDeDestinos[0], listaDeDestinos[2]);