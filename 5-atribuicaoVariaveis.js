console.log("Trabalhando com atribuição de variáveis");

// const idade = 29;
const primeiroNome = "Felipe";
const sobrenome = "Oliveira";

console.log(primeiroNome + sobrenome);
console.log(primeiroNome + " " + sobrenome);
console.log(primeiroNome, sobrenome);
console.log(`Meu nome é ${primeiroNome} ${sobrenome}`);
console.log("Meu nome é " +primeiroNome,sobrenome);
console.log('Meu nome é ' +primeiroNome, sobrenome);

const nomeCompleto = primeiroNome + " " + sobrenome;
console.log("Meu nome é " +nomeCompleto);


let idade; //Declarando variavel
idade = 26; //atribuindo valor a variavel
idade = idade +1;
console.log(idade);