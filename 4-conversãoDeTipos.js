console.log("Conversão de Tipos");
console.log("Ano " + 2020);
console.log("2" + "2");
console.log(parseInt("2") + parseInt("2"))

console.log("10" / "2"); //O js já faz a conversão
console.log("7" / "2"); //O js já faz a conversão
console.log("Felipe" / "2"); //NaN -> Not a Number
console.log(6.5);
console.log(6 , 5);